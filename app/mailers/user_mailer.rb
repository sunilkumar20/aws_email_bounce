class UserMailer < ApplicationMailer

	def welcome(email)
		mail(:to => email, :from => "Info <info@dummy.com>")    
	end
end
